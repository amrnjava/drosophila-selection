# Data
- vcf file: ~/921.vcf  (format: https://samtools.github.io/hts-specs/VCFv4.2.pdf)
    - meta-info: 
    ```
    ##fileformat=VCFv4.1
    ##FILTER=<ID=PASS,Description="All filters passed">
    ##filedate=20190718
    ##source="beagle.jar (r1399)"
    ##INFO=<ID=AF,Number=A,Type=Float,Description="Estimated Allele Frequencies">
    ##INFO=<ID=AR2,Number=1,Type=Float,Description="Allelic R-Squared: estimated correlation between most probable ALT dose and true ALT dose">
    ##INFO=<ID=DR2,Number=1,Type=Float,Description="Dosage R-Squared: estimated correlation between estimated ALT dose [P(RA) + 2*P(AA)] and true ALT dose">
    ##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
    ##FORMAT=<ID=DS,Number=1,Type=Float,Description="estimated ALT dose [P(RA) + P(AA)]">
    ##FORMAT=<ID=GP,Number=G,Type=Float,Description="Estimated Genotype Probability">
    ##contig=<ID=chr2L>
    ##contig=<ID=chr2R>
    ##contig=<ID=chr3L>
    ##contig=<ID=chr3R>
    ##contig=<ID=chr4>
    ##contig=<ID=chrY>
    ##bcftools_annotateVersion=1.3.1+htslib-1.3.1
    ##bcftools_annotateCommand=annotate --set-id +%CHROM\_%POS\_%REF\_%FIRST_ALT --output-type z --output ID_2019-07-18_beagleVersion4_out.HeaderReplaced.vcf.gz 2019
    -07-18_beagleVersion4_out.HeaderReplaced.vcf.gz

    ```
-   14049 indels (~6%)
    -   examples:
    ```
    chr2L   35645   chr2L_35645     AGAG    A       .       PASS    .       GT:DS:GP        0|1:1.06:0.211,0.518,0.271      0|1:1.448:0,0.552,0.448 0|1:1.422:0,0.578,0.422 1|0:0.632:0.368,0.632,0 0|1:1:0,1,0     1|1:1.632:0,0.368,0.632 1|0:0.421:0.579,0.421,0 1|1:1.452:0,0.548,0.452
    chr2L   155955  chr2L_155955    AG      A       .       PASS    .       GT:DS:GP        0|0:0:1,0,0     0|0:0:1,0,0     0|0:0:1,0,0     0|0:0:1,0,0     0|0:0:1,0,0		0|0:0:1,0,0     0|0:0:1,0,0     0|0:0:1,0,0
    chr2L   173243  chr2L_173243    C       CAG     .       PASS    .       GT:DS:GP        0|1:1.144:0.18,0.497,0.324      0|1:1.008:0.246,0.5,0.254       0|0:1.008:0.246,0.5,0.254       1|1:1.631:0,0.369,0.631 1|1:1.496:0,0.504,0.496 1|0:1.127:0.186,0.501,0.313     1|1:1.513:0,0.487,0.513 1|0:1.008:0.246,0.5,0.254
    ```
-   921 samples/individuals
-   vcftools: https://vcftools.github.io/man_latest.html
-   bcftools: http://samtools.github.io/bcftools/bcftools.html

-   allele frequencies
    - before selection (G5)
    - after selection (G11)
        - 27 lines (order of lines in "lines.tbl")
            - each with ~8 individuals ![ninds5](ninds5.png)


# R
- delta_p table:
    - 27 columns (lines)
    - 235772 rows (genomic positions)
    - values: p11-p5 for each line for each position
        - I took values from the last column of .freq file 
```
delta_p<-matrix(,235772,27)
frq_files_G5<-list.files(pattern = "*_gen5.frq")
frq_files_G11<-list.files(pattern = "*_gen11.frq")
frq_files_G5<-frq_files_G5[-6]

for(i in 1:27){
  temp11<-read.table(frq_files_G11[i], row.names = NULL)
  temp5<-read.table(frq_files_G5[i], row.names = NULL)
  dp<-temp11[,6]-temp5[,6]
  delta_p[,i]<-dp
}
```
- basic plots
    - ![hist](hist_mean.png)
    - some positions were not polymorphic in all of the lines, that is, p=0 or p=1 in G5 (fake SNPs)
        - ![fakeSNPs](fake_SNPs.png)
    - histogram after removing fake SNPs:
        - all in one plot: ![hist_all](hist_c_all_lines.png)
        - separately for each line: ![hist_1_9](hist_c_1.png) ![hist_c_2](hist_c_2.png)  ![hist_19_27](hist_c_3.png)
    - ![cdf](cdf_dp.png)
    - ![boxplot](drosophila_boxplot.png) ![filtered_boxplot](filtered_dp.png)
    - ![boxplot_p5](p5_boxplot.png)
    - variance over the lines:
        - ![var_p5](v_p5.png)
        - ![var_dp](v_dp.png)
    - mean over the lines ![mean_dp](mean_dp.png)  ![filtered_mean_dp](filtered_mean_dp.png)
    - delta p for all the lines, filtered 
        - ![dp1](dp_1_9.png) ![dp2](dp_10_18.png) ![dp3](dp_19_27.png)
    - improvised running mean ![rm](rm_dp.png)  ![filtered](filtered.png)
    - delta p squared as a function of MAF: ![dp_maf1](dps_maf_1.png) ![dp_maf2](dps_maf_2.png)
    ![dp_maf3](dps_maf_3.png)

* TO DO:
    - quantify variance over the lines?
    - play with different window sizes

    - compare to the neutral model, drift (check out Signatures of selection paper)

# SLiM

* http://benhaller.com/slim/SLiM_Manual.pdf
*  QtSLiM, a port of SLiMgui to Linux based on the Qt widget kit.  If you’re following along with QtSLiM, you may notice minor differences; however, the two apps are identical in most respects.

* `~/build/QtSLiM`

* preparing vcf file for the test example
    *   `amrnjava@bea81:~$ vcftools --vcf 921.vcf --chr chr2L --keep indslist_line10_gen5.txt --remove-indels --recode --out line10_2L`
* preparing recombination map   
    ```
    rm<-read.csv("drosophila_rec_map_ver5.csv")
    c2l<-rm[rm$Chromosome.Arm=="2L",]
    out<-paste("2L:",c2l$Window.begins, sep = "")
    write.table(out,"coo5.tbl", row.names = F,col.names = F, quote = F)
    ###coordinates converted with FlyBase coordinate converter
    coo6<-read.delim("~/Downloads/FlyBase_Converted_Coordinates.tsv",)
    c6<-substring(coo6$X2L.1.1,4,)
    rm6<-data.frame(Start=c(1, as.integer(c6[-230]),23200001), rec_rate=c2l$CO.rate..cM.Mb.female.meiosis.)
    write.table(rm6,"coo6_2L.tbl", row.names = F,col.names = F, quote = F, sep = "\t")
    ```

*   bash script to run slim simulations in a loop : `~/run_slim.sh`   
        
        ```

            #!/bin/bash

            for((i=1;i<=100;i++))
            do
                sed "s/INDEX/$i/g" droso_neutral_chr2L_line10_generic.slim > droso_neutral_chr2L_line10_specific.slim
                ~/Downloads/build/slim droso_neutral_chr2L_line10_specific.slim
            done

            echo "Done"

        ```

*   simulation results:
![hist_sim](hist_delta_p_sim.png)
![deltap_var](deltap_sim_var.png)