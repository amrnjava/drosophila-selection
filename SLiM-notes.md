* *code completion*: 
    * start typing the command and press the ESC key
* *see function parameters* :
    * click between the parentheses of the function call, and then look at the status bar at the bottom of the window
* *callbacks*
    * `initialize()`
    * `early()`
    * `late()`
* basic output:
    * `10000 late() { sim.outputFull(); }`
        *   It begins with an output prefix, #OUT:, followed by the generation (10000) and the type of output (A for all). The next section describes the subpopulations; p1 is of size 500 and is hermaphroditic (H). Next comes a **list of all mutations present; each line shows one mutation, including (1) a temporary unique identifier used only in this output section, (2) a permanent unique identifier kept by SLiM throughout a run, (3) the mutation type, (4) the base position, (5) the selection coefficient (here always 0 since this is a neutral model), (6) the dominance coefficient (here always 0.5), (7) the identifier of the subpopulation in which the mutation first arose, (8) the generation in which it arose, and (9) the prevalence of the mutation (the number of genomes that contain the mutation, where – in the way that SLiM uses the term “genome” – there are two genomes per individual)**. 
        Next comes a list of individuals.  The first line of this section as shown above, for example, shows that individual 0 in subpopulation 1 (p1:i0) is a hermaphrodite (H) and is comprised of two genomes, p1:0 and p1:1, that will be listed in the following section.  This section makes it easier to figure out which genomes correspond to which individuals, but is largely redundant.
        The final section lists all of the genomes in the simulation.  The first line in this section as shown above, for example, shows that genome p1:0 (which the Individuals section told us belonged to individual p1:i0) is an autosome (A) and contains the mutations with the identifiers 0, 1, 2, ... 15.
    * `outputVCFSample()`
    * output individual genomes : 
        ```
        10000 late() 
        {  
            allIndividuals = sim.subpopulations.individuals;  
            w = asFloat(allIndividuals.countOfMutationsOfType(m2) + 1);  
            sampledIndividuals = sample(allIndividuals, 10, weights=w);  
            sampledIndividuals.genomes.output(); 
        }
        ```
    * output substitutions
        *   `10000 late() { sim.outputFixedMutations(); } `
    * custom output
        *   `100 late() { cat(paste(sim.mutations.position, "\n")); } `
* multiple chromosomes: `initializeRecombinationRate(c(1e-10, 0.5, 1e-10), c(39999, 40000, 99999));`

#   WF and non-WF models
The general trend is that nonWF models are more individual-based, more script-controlled, and potentially more biologically realistic – but also more complex in some respects, because the SLiM core is managing fewer details of the model’s dynamics automatically.  In particular, all nonWF models must implement at least one reproduction()callback in order to generate new offspring.  Each type of model has its appropriate uses; nonWF models are not “better”, although they are more flexible in some respects.  WF models may be simpler to design, and may run somewhat faster; and of course staying within the WF conceptual framework may make it easier to compare simulation results with theoretical expectations from analytical models that are based in the Wright–Fisher paradigm.
#   Qantitative trait models in SLiM (*ch. 13 in the manual*)
*   Since QTLs need to continue to affect phenotype even after they fix, we will need to use the convertToSubstitution property of MutationType to suppress the substitution of the QTL mutations
#   reading from a VCF file
*   Read new mutations from the VCF format file at filePath and add them to the target genomes.  The number of target genomes must match the number of genomes represented in the VCF file (i.e., two times the number of samples, if each sample is diploid).  To read into all of the genomes in a given subpopulation pN, simply call pN.genomes.readFromVCF(), assuming the subpopulation’s size matches that of the VCF file taking ploidy into account.  A vector containing all of the mutations created by readFromVCF() is returned.
SLiM’s VCF parsing is quite primitive.  The header is parsed only inasmuch as SLiM looks to see whether SLiM-specific VCF fields are defined or not; the rest of the header information is ignored.  Call lines are assumed to follow the format:
#CHROM POS ID REF ALT QUAL FILTER INFO FORMAT i0...iN
The CHROM, ID, QUAL, FILTER, and FORMAT fields are ignored, and information in the genotype fields beyond the GT genotype subfield are also ignored.  SLiM’s own VCF annotations are honored; in particular, mutations will be created using the given values of MID, S, PO, GO, and MT if those subfields are present, and DOM, if it is present, must match the dominance coefficient of the mutation type.  The parameter mutationType (a MutationType object or id) will be used for any mutations that have no supplied mutation type id in the MT subfield; if mutationType would be used but is NULL an error will result.  Mutation IDs supplied in MID will be used if no mutation IDs have been used in the simulation so far; if any have been used, it is difficult for SLiM to guarantee that there are no conflicts, so a warning will be emitted and the MID values will be ignored.  If selection coefficients are not supplied with the S subfield, they will be drawn from the mutation type used for the mutation.  If a population of origin is not supplied with the PO subfield, -1 will be used.  If a generation of origin is not supplied with the GO subfield, the current generation will be used.
REF and ALT must always be comprised of simple nucleotides (A/C/G/T) rather than values representing indels or other complex states.  Beyond this, the handling of the REF and ALT fields depends upon several factors.  First of all, these fields are ignored in non-nucleotide-based models, although they are still checked for conformance.  In nucleotide-based models, when a header definition for SLiM’s NONNUC tag is present (as when nucleotide-based output is generated by SLiM): Second, if a NONNUC field is present in the INFO field the call line is taken to represent a non-nucleotide-based mutation, and REF and ALT are again ignored.  In this case the mutation type used must be non-nucleotide-based.  Third, if NONNUC is not present the call line is taken to represent a nucleotide-based mutation.  In this case, the mutation type used must be nucleotide-based.  Also, in this case, the specified reference nucleotide must match the existing ancestral nucleotide at the given position.  In nucleotide-based models, when a header definition for SLiM’s NONNUC tag is not present (as when loading a non-SLiM-generated VCF file): The mutation type will govern the way nucleotides are handled.  If the mutation type used for a mutation is nucleotide-based, the nucleotide provided in the VCF file for that allele will be used.  If the mutation type is non-nucleotide-based, the nucleotide provided will be ignored.
If multiple alleles using the same nucleotide at the same position are specified in the VCF file, a separate mutation will be created for each, mirroring SLiM’s behavior with independent mutational lineages when writing VCF.  The MULTIALLELIC flag is ignored by readFromVCF(); call lines for mutations at the same base position in the same genome will result in stacked mutations whether or not MULTIALLELIC is present.
The target genomes correspond, in order, to the haploid or diploid calls provided for i0…iN (the sample IDs) in the VCF file.  In sex-based models that simulate the X or Y chromosome, null genomes in the target vector will be skipped, and will not be used to correspond to any of i0…iN; however, care should be taken in this case that the genomes in the VCF file correspond to the target genomes in the manner desired.

#   running simulations from the command line
    ` ~/Downloads/build/slim simulation.slim `